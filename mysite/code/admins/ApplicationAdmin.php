<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 9/10/15
 * Time: 20:05
 */
class ApplicationAdmin extends ModelAdmin {

    private static $url_segment = 'application';

    private static $menu_title = '项目管理';

    private static $managed_models = array(
        'Application'
    );

    private $status;

    private $menu_items = array();

    public function init() {
        parent::init();
        if(!Member::currentUserID()) {
            return Security::permissionFailure();
        }
        //Set up menu items
        $groupConfig =  Member::currentUser()->getLTGroupConfig();
        if($groupConfig) {
            $this->menu_items = $groupConfig['status_menu']['application'];
        }
        //To setup order type var
        if(($status = $this->request->requestVar('s'))) {
            $this->status = $status;
        } else {
            reset($this->menu_items);
            $this->status = count($this->menu_items) ? key($this->menu_items): false;
        }

        CMSMenu::remove_menu_item('Help');
    }

    public function getManagedModelTabs() {
        $class = 'Application';
        $tabs = new ArrayList();

        if(count($this->menu_items)) {
            foreach ($this->menu_items as $status => $label) {
                $menuData = new ArrayData(array(
                    'Title'     => $label,
                    'ClassName' => $class,
                    'Link' => $this->Link($this->sanitiseClassName($class)) . "?s=" . $status,
                    'LinkOrCurrent' => ($this->status == $status) ? 'current' : 'link'
                ));
                $tabs->push($menuData);
            }

            return $tabs;
        }

        return parent::getManagedModelTabs();
    }

    public function getList() {
        $list = parent::getList();
        if (Permission::check('ADMIN')) {
            return $list;
        }

        if (!Permission::check('APPLICATION_ALL_MEMBER')) {
            $list = $list->filter(array(
                'MemberID' => Member::currentUserID()
            ));
        }

        $list = $list->filter(array(
            'Status' => $this->status
        ));

        return $list;
    }

    public function getEditForm($id = null, $fields = null) {
        $form = parent::getEditForm($id, $fields);
        $form->Fields()->dataFieldByName($this->modelClass)->getConfig()
            ->removeComponentsByType('GridFieldExportButton')
            ->removeComponentsByType('GridFieldPrintButton');

        $canCreateStatus = Member::currentUser()->inGroup('organization') ? 'PENDING_SUBMIT_OFFICE' : 'PENDING_SUBMIT_ORG';
        if($canCreateStatus !== $this->status) {
            $form->Fields()->dataFieldByName('Application')->getConfig()->removeComponentsByType('GridFieldAddNewButton');
        }
        $formAction = $form->FormAction() . '?s=' . $this->status;
        $form->setFormAction($formAction);

        return $form;
    }
}
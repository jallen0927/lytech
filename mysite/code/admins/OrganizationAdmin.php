<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 27/03/16
 * Time: 22:24
 */
class OrganizationAdmin extends ModelAdmin {
    private static $url_segment = 'organization';

    private static $menu_title = '企业管理';

    private static $managed_models = array(
        'Organization'
    );

    public function init() {
        parent::init();
        CMSMenu::remove_menu_item('Help');
    }

    public function getEditForm($id = null, $fields = null) {
        $form = parent::getEditForm($id, $fields);
        $form->Fields()->dataFieldByName($this->modelClass)->getConfig()
            ->removeComponentsByType('GridFieldExportButton')
            ->removeComponentsByType('GridFieldPrintButton');

        return $form;
    }
}
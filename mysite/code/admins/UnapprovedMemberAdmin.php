<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 24/04/16
 * Time: 20:04
 */
class UnapprovedMemberAdmin extends ModelAdmin {

    private static $managed_models = array(
        'UnapprovedMember'
    );

    private static $menu_title = '账号审核';

    private static $url_segment = 'member-approval';

    public function getList() {
        if(Permission::check('ADMIN')) {
            return parent::getList();
        }
        return parent::getList()->filter(array(
            'Approved' => false
        ));
    }

    public function getEditForm($id = null, $fields = null) {
        $form = parent::getEditForm($id, $fields);
        $form->Fields()->dataFieldByName($this->modelClass)->getConfig()
            ->removeComponentsByType('GridFieldExportButton')
            ->removeComponentsByType('GridFieldPrintButton');

        return $form;
    }
}
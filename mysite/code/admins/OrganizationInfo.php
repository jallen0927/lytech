<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 18/04/16
 * Time: 18:14
 */
class OrganizationInfo extends LeftAndMain {

    private static $url_segment = 'organizationinfo';

    private static $menu_title = '企业信息';

    private static $tree_class = 'Organization';

    private static $required_permission_codes = array('CMS_ACCESS_OrganizationInfo');

    public function init() {
        parent::init();
        CMSMenu::remove_menu_item('Help');
    }

    public function getEditForm($id = null, $fields = null) {
        $organization = Member::currentUser()->Organization();

        $fields = $organization->getCMSFields();

        // Tell the CMS what URL the preview should show
        $home = Director::absoluteBaseURL();
        $fields->push(new HiddenField('PreviewURL', 'Preview URL', $home));

        // Added in-line to the form, but plucked into different view by LeftAndMain.Preview.js upon load
        $fields->push($navField = new LiteralField('SilverStripeNavigator', $this->getSilverStripeNavigator()));
        $navField->setAllowHTML(true);

        // Retrieve validator, if one has been setup (e.g. via data extensions).
        if ($organization->hasMethod("getCMSValidator")) {
            $validator = $organization->getCMSValidator();
        } else {
            $validator = null;
        }

        $actions = $organization->getCMSActions();

        $form = CMSForm::create(
            $this, 'EditForm', $fields, $actions, $validator
        )->setHTMLID('Form_EditForm');
        $form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-content center cms-edit-form');
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        if($form->Fields()->hasTabset()) $form->Fields()->findOrMakeTab('Root')->setTemplate('CMSTabSet');
        $form->setHTMLID('Form_EditForm');
        $form->loadDataFrom($organization);
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));

        // Use <button> to allow full jQuery UI styling
        $actions = $actions->dataFields();
        if($actions) foreach($actions as $action) $action->setUseButtonTag(true);

        $this->extend('updateEditForm', $form);

        return $form;
    }

    public function save_orgnization($data, $form) {
        $organization = Member::currentUser()->Organization();
        $form->saveInto($organization);

        try {
            $organization->write();
        } catch(ValidationException $ex) {
            $form->sessionMessage($ex->getResult()->message(), 'bad');
            return $this->getResponseNegotiator()->respond($this->request);
        }

        $this->response->addHeader('X-Status', rawurlencode(_t('LeftAndMain.SAVEDUP', 'Saved.')));

        return $form->forTemplate();
    }


    public function getSilverStripeNavigator() {
        return $this->renderWith('CMSSettingsController_SilverStripeNavigator');
    }


    public function Breadcrumbs($unlinked = false) {
        $defaultTitle = self::menu_title_for_class(get_class($this));

        return new ArrayList(array(
            new ArrayData(array(
                'Title' => _t("{$this->class}.MENUTITLE", $defaultTitle),
                'Link' => false
            ))
        ));
    }

}
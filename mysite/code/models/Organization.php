<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 9/10/15
 * Time: 19:38
 */
class Organization extends DataObject {

    private static $singular_name = '企业';

    private static $plural_name = '企业';

    private static $has_many = array(
        'Members' => 'Member'
    );

    private static $has_one = array(
        'Account' => 'Member'
    );

    private static $summary_fields = array(
        'company_name' => '企业名称',
        'establish_time' => '成立时间',
        'industry_class' => '行业类别',
        'register_time' => '注册时间'
    );

    private static $field_labels = array(
        'company_name' => '企业名称',
        'establish_time' => '成立时间',
        'company_address' => '企业地址',
        'industry_class' => '行业类别',
        'register_time' => '注册时间',
        'registered_capital' => '注册资本',
        'company_code' => '组织机构代码',
        'corporate_rep' => '法人代表',
        'rep_phone' => '联系方式	',
        'rep_title' => '职务/职称',
        'contact' => '联系人',
        'contact_phone' => '联系方式',
        'contact_title' => '职务/职称',
        'fax' => '传真',
        'website' => '网址',
        'email' => '电子邮箱',
        'floor_space' => '占地面积',
        'revenue' => '营业收入',
        'work_force' => '职工总数',
        'high_work_force' => '大专以上职工数',
        'tax_revenue' => '利税收入',
        'pay_tax' => '上缴税额',
        'hightech_revenue' => '高新技术产品、新产品等销售收入',
        'company_class' => '企业类别',
        'research_class' => '研发中心类别',
        'tech_member' => '科技人员数',
        'jishuyituodanwei' => '技术依托单位',
        'rd_sale_ratio' => '研发费占销售收入比',
        'invent_patent' => '发明专利数 - 当年',
        'invent_patent_all' => '发明专利数 - 累计',
        'practical_patent' => '实用新型专利 - 当年',
        'practical_patent_all' => '实用新型专利 - 累计',
        'facade_patent' => '外观设计专利数 - 当年',
        'facade_patent_all' => '外观设计专利数 - 累计',
        'business_scope' => '经营范围',
        'tech_awards' => '科技成果登记及获奖情况',
        'high_product' => '高新技术产品和新产品',
        'patent_invent' => '发明专利',
        'patent_practical' => '实用新型专利',
        'patent_appear' => '外观设计专利',
        'tech_coorperation' => '科企合作',
        'tech_introduce' => '科企合作	',
        'orientation_other' => '其他',
        'research_construction' => '研发中心建设',
        'tech_upgrade' => '科技型企业升级'
    );

    private static $required_fields = array(
        'company_name',
        'establish_time',
        'industry_class'
    );

    public function getTitle() {
        return $this->company_name;
    }

    public function isComplete() {
        $flag = true;
        foreach (self::$required_fields as $field) {
            $flag = $flag && $field;
        }

        return $flag;
    }

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->removeByName('Members');
        $fields->removeByName('AccountID');
        $fields->dataFieldByName('establish_time')->setConfig('showcalendar', true);
        $fields->dataFieldByName('register_time')->setConfig('showcalendar', true);
        $fields->replaceField('company_name', new ReadonlyField('company_name', '企业名称'));

        $fields->replaceField('company_class', CheckboxSetField::create('company_class', '企业类别', array(
                '高新技术企业 - 国家' => '高新技术企业 - 国家',
                '高新技术企业 - 市' => '高新技术企业 - 市',
                '省级创新型示范企业' => '省级创新型示范企业',
                '省级创新型试点企业' => '省级创新型试点企业',
                '省级科技型中小企业' => '省级科技型中小企业',
                '省级专利示范企业' => '省级专利示范企业',
                '省级农业科技型企业' => '省级农业科技型企业',
                '市级专利示范企业' => '市级专利示范企业',
                '市级创新型企业' => '市级创新型企业',
                '县级科技型企业' => '县级科技型企业',
                '其他' => '其他'
            )));
        $fields->replaceField('research_class', CheckboxSetField::create('research_class', '研发中心类别', array(
                '省级企业研究院' => '省级企业研究院',
                '省级高新技术企业研发中心' => '省级高新技术企业研发中心',
                '省级农业企业科技研发中心' => '省级农业企业科技研发中心',
                '省级区域科技创新服务中心' => '省级区域科技创新服务中心',
                '省级大院名校共建创新载体' => '省级大院名校共建创新载体',
                '市级工程技术研究开发中心' => '市级工程技术研究开发中心',
                '企业所属研发机构' =>	'企业所属研发机构',
                '其他' => '其他'
        )));

        $fields->replaceField('research_construction', CheckboxSetField::create('research_construction', '研发中心建设', array(
            '国家级' => '国家级',
            '省级' => '省级',
            '市级' => '市级',
            '县级' => '县级'
        )));

        $fields->replaceField('tech_upgrade', CheckboxSetField::create('tech_upgrade', '科技型企业升级', array(
            '国家级' => '国家级',
            '省级' => '省级',
            '市级' => '市级',
            '县级' => '县级'
        )));

        $fields->addFieldToTab('Root.Main', LiteralField::create('企业规模和占地情况', '<h3>企业规模和占地情况</h3>'), 'floor_space');
        $fields->addFieldToTab('Root.Main', LiteralField::create('科研活动情况', '<h3>科研活动情况</h3>'), 'tech_member');
        $fields->addFieldToTab('Root.Main', LiteralField::create('科技创新项目意向', '<h3>科技创新项目意向</h3>'), 'patent_invent');

        return $fields;
    }

    public function getCMSValidator() {
        return RequiredFields::create(self::$required_fields);
    }

    public function getCMSActions() {
        if (Permission::check('ADMIN') || Permission::check('EDIT_ORGANIZATIONINFO')) {
            $actions = new FieldList(
                FormAction::create('save_orgnization', _t('CMSMain.SAVE','Save'))
                    ->addExtraClass('ss-ui-action-constructive')->setAttribute('data-icon', 'accept')
            );
        } else {
            $actions = new FieldList();
        }

        $this->extend('updateCMSActions', $actions);

        return $actions;
    }

    public function canCreate($member = null) {
        return false;
    }

    public function canDelete($member = null) {
        if(Permission::check('ADMIN')) {
            return true;
        }
        return false;
    }

    private static $db = array(
        'company_name' => 'Varchar(255)',
        'establish_time' => 'Date',
        'company_address' => 'Varchar(255)',
        'industry_class' => 'Varchar(255)',
        'register_time' => 'Date',
        'registered_capital' => 'Varchar(255)',
        'company_code' => 'Varchar(255)',
        'corporate_rep' => 'Varchar(255)',
        'rep_phone' => 'Varchar(255)',
        'rep_title' => 'Varchar(255)',
        'contact' => 'Varchar(255)',
        'contact_phone' => 'Varchar(255)',
        'contact_title' => 'Varchar(255)',
        'fax' => 'Varchar(255)',
        'website' => 'Varchar(255)',
        'email' => 'Varchar(255)',
        'business_scope' => 'Text',
        'floor_space' => 'Varchar(255)',
        'revenue' => 'Varchar(255)',
        'work_force' => 'Int',
        'high_work_force' => 'Int',
        'tax_revenue' => 'Varchar(255)',
        'pay_tax' => 'Varchar(255)',
        'hightech_revenue' => 'Varchar(255)',
        'company_class' => 'Varchar(255)',
        'research_class' => 'Varchar(255)',
        'tech_member' => 'Int',
        'jishuyituodanwei' => 'Int',
        'rd_sale_ratio' => 'Varchar(255)',
        'invent_patent' => 'Int',
        'invent_patent_all' => 'Int',
        'practical_patent' => 'Int',
        'practical_patent_all' => 'Int',
        'facade_patent' => 'Int',
        'facade_patent_all' => 'Int',
        'tech_awards' => 'Int',
        'high_product' => 'Int',
        'patent_invent' => 'Int',
        'patent_practical' => 'Int',
        'patent_appear' => 'Int',
        'research_construction' => 'Varchar(255)',
        'tech_upgrade' => 'Varchar(255)',
        'tech_coorperation' => 'Int',
        'tech_introduce' => 'Int',
        'orientation_other' => 'Int'
    );

}
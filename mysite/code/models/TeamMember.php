<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 20/03/16
 * Time: 21:02
 */
class TeamMember extends DataObject {
    private static $singular_name = '项目组成员';

    private static $plural_name = '项目组成员';

    private static $db = array(
        'Name' => 'Varchar',
        'DOB' => 'Date',
        'Duty' => 'Varchar(255)',
        'Major' => 'Varchar',
        'Company' => 'Varchar',
        'Responsibility' => 'Varchar'
    );

    private static $field_labels = array(
        'Name' => '姓名',
        'DOB' => '出生年月',
        'Duty' => '专业技术职务',
        'Major' => '专业',
        'Company' => '工作单位',
        'Responsibility' => '在本项目中分工',
        'Application' => '项目申请'
    );

    private static $summary_fields = array(
        'Name' => '姓名',
        'Duty' => '专业技术职务',
        'Responsibility' => '在本项目中分工'
    );

    private static $has_one = array(
        'Application' => 'Application'
    );

    public function canView($member = null) {
        return true;
    }

    public function canCreate($member = null) {
        return true;
    }

    public function canEdit($member = null) {
        return true;
    }

    public function canDelete($member = null) {
        return true;
    }

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->dataFieldByName('DOB')->setConfig('showcalendar', true);

        return $fields;
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 20/03/16
 * Time: 20:46
 */
class Cooperator extends DataObject {
    private static $singular_name = '合作单位';

    private static $plural_name = '合作单位';

    private static $db = array(
        'Title' => 'Varchar',
        'Representative' => 'Varchar',
        'Duty' => 'Varchar'
    );

    private static $has_one = array(
        'Application' => 'Application'
    );

    private static $field_labels = array(
        'Title' => '单位名称',
        'Representative' => '法人代表',
        'Duty' => '职责*(*: 0-承担，1－参加)',
        'Application' => '项目申请'
    );

    public function canView($member = null) {
        return true;
    }

    public function canCreate($member = null) {
        return true;
    }

    public function canEdit($member = null) {
        return true;
    }

    public function canDelete($member = null) {
        return true;
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 9/10/15
 * Time: 19:38
 */
class Application extends DataObject implements PermissionProvider {

    private static $singular_name = '项目申请';

    private static $plural_name = '项目申请';

    private static $has_one = array(
        'Member' => 'Member',
        'Diagram' => 'Image',
        'FeasibilityReport' => 'File'
    );

    private static $has_many = array(
        'Cooperators' => 'Cooperator',
        'TeamMembers' => 'TeamMember',
        'Schedules' => 'Schedule'
    );

    private static $summary_fields = array(
        'Name' => '项目名称',
        'Member.Organization.company_name' => '单位名称',
        'Created' => '建立时间'
    );

    public function getCMSValidator() {
        return RequiredFields::create(array(
            'Name',
            'project_class',
            'industry_type',
            'tech_resource',
            'start_date',
            'finish_date'
        ));
    }

    public function providePermissions() {
        return array(
            'APPLICATION_EDIT_PENDING_ORG_ASSESSING' => array(
                'name' => '查看待审核项目',
                'help' => '',
                'category' => '项目权限',
                'sort' => 100
            )
        );
    }
    public function canView($member = null) {
        if(!$this->ID) {
            return $this->canCreate($member);
        }
        return Permission::check(strtoupper($this->ClassName) . '_VIEW_' . $this->getField('Status'));
    }

    public function canEdit($member = null) {
        if(!$this->ID) {
            return $this->canCreate($member);
        }
        return Permission::check(strtoupper($this->ClassName) . '_EDIT_' . $this->getField('Status'));
    }

    public function canCreate($member = null) {
        return Permission::check(strtoupper($this->ClassName) . '_CREATE')
        && Member::currentUser()->findOrCreateOrganization()->isComplete();
    }

    public function canDelete($member = null) {
        return Permission::check(strtoupper($this->ClassName) . '_DELETE_' . $this->getField('Status'));
    }

    public function canSubmitOrg() {
        return Permission::check(strtoupper($this->ClassName) . '_submitOrg_' . $this->getField('Status'));
    }

    public function submitOrg() {
        $this->setField('Status', 'PENDING_ORG_ASSESSING');
        $this->write();
    }

    public function canReturnRevising() {
        return Permission::check(strtoupper($this->ClassName) . '_returnRevising_' . $this->getField('Status'));
    }

    public function returnRevising() {
        $this->setField('Status', 'REVISING');
        $this->write();
    }

    public function submitOffice() {
        $this->setField('Status', 'PENDING_OFFICE_ASSESSING');
        $this->write();
    }

    public function canOfficeApprove() {
        return Permission::check(strtoupper($this->ClassName) . '_officeApprove_' . $this->getField('Status'));
    }

    public function officeApprove() {
        $this->setField('Status', 'PENDING_ADMIN_ASSESSING');
        $this->write();
    }

    public function canAdminApprove() {
        return Permission::check(strtoupper($this->ClassName) . '_adminApprove_' . $this->getField('Status'));
    }

    public function adminApprove() {
        $this->setField('Status', 'APPROVED');
        $this->write();
    }

    public function canInitiate() {
        return Permission::check(strtoupper($this->ClassName) . '_initiate_' . $this->getField('Status'));
    }

    public function initiate() {
        $this->setField('Status', 'INITIATED');
        $this->write();
    }

    public function canFail() {
        return Permission::check(strtoupper($this->ClassName) . '_fail_' . $this->getField('Status'));
    }

    public function fail() {
        $this->setField('Status', 'UNSUCCESSFUL');
        $this->write();
    }

    public function onBeforeWrite() {
        parent::onBeforeWrite();
        if(!$this->ID) {
            $this->MemberID = Member::currentUserID();
            $this->Status = Member::currentUser()->inGroup('organization') ? 'PENDING_SUBMIT_OFFICE' : 'PENDING_SUBMIT_ORG';
        }
    }

    public function getCMSFields() {
        $originalFields = parent::getCMSFields();
        $fields = new FieldList();
        $fields->push(new TabSet('Root', new Tab('Main')));

        $fields->addFieldsToTab('Root.Main', array(
            TextField::create('Name', '项目名称'),
            GroupedDropdownField::create('project_class', '项目类别', self::$class_types)->setEmptyString('请选择'),
            DateField::create('start_date', '开始日期')->setConfig('showcalendar', true),
            DateField::create('finish_date', '结束日期')->setConfig('showcalendar', true),
            DropdownField::create('tech_resource', '项目技术来源', $this->dbObject('tech_resource')->enumValues())->setEmptyString('请选择'),
            DropdownField::create('industry_type', '项目计划类别', $this->dbObject('industry_type')->enumValues())->setEmptyString('请选择'),
        ));

        if ($this->ID) {
            $cooperatorsField = $originalFields->dataFieldByName('Cooperators')->setTitle('合作单位');
            $cooperatorsField->getConfig()->removeComponentsByType('GridFieldDeleteAction');
            $teamMembersField = $originalFields->dataFieldByName('TeamMembers')->setTitle('项目组成员');
            $teamMembersField->getConfig()->removeComponentsByType('GridFieldDeleteAction');
            $scheduleField = $originalFields->dataFieldByName('Schedules')->setTitle('计划进度');
            $scheduleField->getConfig()->removeComponentsByType('GridFieldDeleteAction');
            $cooperatorsField->getConfig()->removeComponentsByType('GridFieldAddExistingAutocompleter');
            $teamMembersField->getConfig()->removeComponentsByType('GridFieldAddExistingAutocompleter');
            $scheduleField->getConfig()->removeComponentsByType('GridFieldAddExistingAutocompleter');

            $mainFields = array();
            if ($this->industry_type === '重点科技计划项目') {
                $mainFields[] = UploadField::create('FeasibilityReport', '可行性报告')
                    ->setDescription(
                        '龙游县重点科技项目可行性报告及经费概算编写提纲
                             一、项目可行性报告   
                            （一）立项的背景和意义。  
                            （二）国内外研究现状和发展趋势。  
                            （三）项目主要研究开发内容、技术关键及主要创新点。  
                            （四）项目预期目标（主要技术经济指标、社会效益、技术应用和产业化前景以及获取自主知识产权的情况）。
                            （五）项目实施方案、技术路线、组织方式与课题分解。
                            （六）计划进度安排。
                            （七）现有工作基础和条件。
                            二、经费概算
                            凡申请财政分期补助、事先立项事后补助的项目，均应当编制科研项目经费概算。
                            经费概算包括两部分：一是经费概算列表，二是经费概算说明。经费概算列表的表式见该提纲附表“县级科技计划项目经费概算表”。
                            经费概算说明包括： 对承担单位和相关部门承诺提供的支撑条件进行说明；
                            对各科目支出的主要用途、与项目研究的相关性、概算方法、概算依据进行分析说明；对其他来源经费进行说明。
                            项目可行性报告及经费概算编写应当回避项目申报单位和项目负责人、成员相关的信息，否则作无效申报处置。'
                    );
                $mainFields[] = LiteralField::create('FeasibilityDoc',
                    '<a href="">点击下载可行性报告表格</a>'
                );
            }

            $mainFields[] = CompositeField::create(
                LiteralField::create('OverviewInfo', '<h2>项目概览</h2>'),
                LiteralField::create('Budget', '<h3>项目经费预算（万元）</h3>'),
                TextField::create('total_expenditure', '总计'),
                LiteralField::create('Including', '其中包括'),
                TextField::create('zichou', '自筹'),
                TextField::create('yinhangdaikuan', '银行贷款'),
                TextField::create('caizhengshenqing', '向县财政申请'),
                TextField::create('other_expenditure', '其他'),
                LiteralField::create('ExpenseBudget', '<h3>项目经费开支预算（万元）</h3>'),
                TextField::create('shebeifei', '设备费'),
                TextField::create('cailiaofei', '材料费'),
                TextField::create('shiyanhuayanjiagongfei', '试验化验加工费'),
                TextField::create('ranliaodonglifei', '燃料动力费'),
                TextField::create('chailvfei', '差旅费'),
                TextField::create('renyuanlaowufei', '人员劳务费'),
                TextField::create('hezuojiaoliufei', '合作、协作研究与交流费'),
                TextField::create('zhishichanquanshiwufei', '出版/文献/信息传播知识产权事务费'),
                TextField::create('huiyifei', '会议费'),
                TextField::create('guanlifei', '管理费'),
                TextField::create('zhuanjiazixunfei', '专家咨询费'),
                TextField::create('other_spending', '其他开支'),
                LiteralField::create('Expectation', '<h3>预计经济效益</h3>'),
                TextField::create('nianzengchanzhi', '年增产值（万元）'),
                TextField::create('nianzenglirun', '年增利润（万元）'),
                TextField::create('nianzengshuijin', '年增税金（万元）'),
                TextField::create('nianchuanghui', '年创汇（万美元）'),
                TextField::create('nianjiehui', '年节汇（万美元）'),
                LiteralField::create('OtherAchievement', '<h3>预计其他成果</h3>'),
                TextField::create('lunwenshu', '论文数'),
                TextField::create('zhuanli', '专利'),
                TextField::create('famingzhuanli', '其中发明专利'),
                TextareaField::create('beizhu', '备注')
            );

            $fields->addFieldsToTab('Root.Main', $mainFields);

            $fields->addFieldsToTab('Root.Units', array(
                LiteralField::create('PrimaryUnit', '<h3>第一申请单位</h3>'),
                TextField::create('danweimingcheng', '单位名称'),
                TextField::create('danweijiancheng', '单位简称'),
                TextField::create('farendaibiao', '法人代表'),
                TextField::create('suozaididaima', '所在地代码'),
                TextField::create('danweileixing', '单位类型'),
                TextField::create('xiangxidizhi', '详细地址'),
                TextField::create('youzhengbianma', '邮政编码'),
                TextField::create('danweiemail', '单位EMAIL'),
                TextField::create('lianxiren', '联系人'),
                TextField::create('lianxidianhua', '联系电话'),
                TextField::create('chuanzhen', '传真'),
                TextField::create('kaihuyinhang', '开户银行'),
                TextField::create('yinhangzhanghao', '银行帐号'),
                TextField::create('zhuguanbumen', '主管部门'),

                LiteralField::create('CooperatorsHead', '<h3>其他合作单位</h3>'),
                TextField::create('hezuodanweizongshu', '合作单位总数 △：（包含第一申请单位）'),
                TextField::create('chengdandanweishu', '承担单位数'),
                TextField::create('canjiadanweishu', '参加单位数'),
                $cooperatorsField->setTitle('合作单位')
            ));

            $fields->addFieldsToTab('Root.TeamMembers', array(
                LiteralField::create('Leader', '<h3>项目负责人</h3>'),
                TextField::create('fuzeren_name', '姓名'),
                TextField::create('fuzeren_shenfenzheng', '身份证号码'),
                TextField::create('fuzeren_phone', '联系电话'),
                TextField::create('fuzeren_email', 'EMAIL'),
                TextField::create('fuzeren_xueli', '学历'),
                TextField::create('fuzeren_xuewei', '学位'),
                TextField::create('fuzeren_zhiwu', '专业技术职务'),
                TextField::create('fuzeren_zhuanye', '专业'),
                TextField::create('fuzeren_fengong', '在本项目中的分工'),
                TextField::create('fuzeren_danwei', '工作单位'),
                LiteralField::create('TeamMembersHeader', '<h3>项目组成员</h3>'),
                $teamMembersField
            ));

            $fields->addFieldsToTab('Root.Basis', array(
                LiteralField::create('Basis', '四、本项目的立题依据：包括目的、意义、国内外概况和发展趋势，现有工作基础和条件（包括研究工作基础、装备条件和技术力量及项目负责人技术工作简历）'),
                TextareaField::create('litiyiju', '')->setRows(30)
            ));

            $fields->addFieldsToTab('Root.Expectation', array(
                LiteralField::create('Expectation', '五、研究、开发内容和预期成果：具体研究、开发内容和重点解决的技术关键问题，要达到的主要技术、经济指标及经济社会环境效益，拟采取的研究方法和技术路线或工艺流程（可用框图表示）'),
                TextareaField::create('neirongheyuqichengguo', '')->setRows(30),
                UploadField::create('Diagram', '框图')
            ));

            $fields->addFieldToTab('Root.Schedules', $scheduleField);

            $fields->addFieldsToTab('Root.Resource', array(
                TextareaField::create('shichangziyuan', '七、市场资源')->setRows(8)
            ));

            $fields->addFieldsToTab('Root.Budget', array(
                TextareaField::create('shenqingjingfei', '八、项目申请经费：（计算根据及理由）')->setRows(8)
            ));

            $fields->findOrMakeTab('Root.Main')->setTitle('一、基本情况');
            $fields->findOrMakeTab('Root.Units')->setTitle('二、承担单位');
            $fields->findOrMakeTab('Root.TeamMembers')->setTitle('三、项目组成员');
            $fields->findOrMakeTab('Root.Basis')->setTitle('四、立题依据');
            $fields->findOrMakeTab('Root.Expectation')->setTitle('五、内容成果');
            $fields->findOrMakeTab('Root.Schedules')->setTitle('六、计划进度');
            $fields->findOrMakeTab('Root.Resource')->setTitle('七、市场资源');
            $fields->findOrMakeTab('Root.Budget')->setTitle('八、申请经费');
        }
        //For testing
        if (Permission::check('ADMIN')) {
            $fields->addFieldToTab('Root.Test', DropdownField::create('Status', 'Status', self::singleton()->dbObject('Status')->enumValues()));
            $fields->addFieldToTab('Root.Test', DropdownField::create('MemberID', 'Member', Member::get()->map('ID', 'Title'))->setEmptyString(''));
        }

        return $fields;

    }

    private static $db = array(
        'Name' => 'Varchar',
        'ApplicationCode' => 'Varchar',
        'Status' => 'Enum(array("PENDING_SUBMIT_ORG", "PENDING_ORG_ASSESSING", "REVISING", "PENDING_SUBMIT_OFFICE", "PENDING_OFFICE_ASSESSING", "PENDING_ADMIN_ASSESSING", "APPROVED", "INITIATED", "UNSUCCESSFUL"))',
        'project_class' => 'Varchar',
        'industry_type' => "Enum(array('重点科技计划项目', '一般科技计划项目'))",
        'tech_resource' => "Enum(array(
                '自主开发','产学研联合攻关','省内其他单位技术','引进省外、国外技术消化创新','专利技术产业化'
            ))",
        'start_date' => 'Varchar',
        'finish_date' => 'Varchar',
        'total_expenditure' => 'Varchar',
        'zichou' => 'Varchar',
        'yinhangdaikuan' => 'Varchar',
        'caizhengshenqing' => 'Varchar',
        'other_expenditure' => 'Varchar',
        'shebeifei' => 'Varchar',
        'cailiaofei' => 'Varchar',
        'shiyanhuayanjiagongfei' => 'Varchar',
        'ranliaodonglifei' => 'Varchar',
        'chailvfei' => 'Varchar',
        'renyuanlaowufei' => 'Varchar',
        'hezuojiaoliufei' => 'Varchar',
        'zhishichanquanshiwufei' => 'Varchar',
        'huiyifei' => 'Varchar',
        'guanlifei' => 'Varchar',
        'zhuanjiazixunfei' => 'Varchar',
        'other_spending' => 'Varchar',
        'nianzengchanzhi' => 'Varchar',
        'nianzenglirun' => 'Varchar',
        'nianzengshuijin' => 'Varchar',
        'nianchuanghui' => 'Varchar',
        'nianjiehui' => 'Varchar',
        'lunwenshu' => 'Varchar',
        'zhuanli' => 'Varchar',
        'famingzhuanli' => 'Varchar',
        'beizhu' => 'Varchar',
        'danweimingcheng' => 'Varchar',
        'danweijiancheng' => 'Varchar',
        'farendaibiao' => 'Varchar',
        'suozaididaima' => 'Varchar',
        'danweileixing' => 'Varchar',
        'xiangxidizhi' => 'Varchar',
        'youzhengbianma' => 'Varchar',
        'danweiemail' => 'Varchar',
        'lianxiren' => 'Varchar',
        'lianxidianhua' => 'Varchar',
        'chuanzhen' => 'Varchar',
        'kaihuyinhang' => 'Varchar',
        'yinhangzhanghao' => 'Varchar',
        'zhuguanbumen' => 'Varchar',
        'hezuodanweizongshu' => 'Varchar',
        'chengdandanweishu' => 'Varchar',
        'canjiadanweishu' => 'Varchar',
        'fuzeren_name' => 'Varchar',
        'fuzeren_shenfenzheng' => 'Varchar',
        'fuzeren_phone' => 'Varchar',
        'fuzeren_email' => 'Varchar',
        'fuzeren_xueli' => 'Varchar',
        'fuzeren_xuewei' => 'Varchar',
        'fuzeren_zhiwu' => 'Varchar',
        'fuzeren_zhuanye' => 'Varchar',
        'fuzeren_fengong' => 'Varchar',
        'fuzeren_danwei' => 'Varchar',
        'litiyiju' => 'Text',
        'neirongheyuqichengguo' => 'Text',
        'shichangziyuan' => 'Text',
        'shenqingjingfei' => 'Text'
    );

    private static $field_labels = array(
        'Name' => '项目名称',
        'ApplicationCode' => '县科技局编号'
    );

    private static $class_types = array(
        "农、林、牧、渔业" => array(
            "A01" => "农业",
            "A02" => "林业",
            "A03" => "畜牧业",
            "A04" => "渔业",
            "A05" => "农、林、牧、渔服务业"
        ),
        "采掘业" => array(
            "B06" => "煤炭采选业",
            "B07" => "石油和天然气开采业",
            "B08" => "黑色金属矿采选业",
            "B09" => "有色金属矿采选业",
            "B10" => "非金属矿采选业",
            "B11" => "其他矿采选业",
            "B12" => "木材及竹材采运业"
        ),
        "制造业" => array(
            "C13" => "食品加工业",
            "C14" => "食品制造业",
            "C15" => "饮料制造业",
            "C16" => "烟草加工业",
            "C17" => "纺织业",
            "C18" => "服装及其他纤维制品制造业",
            "C19" => "皮革、毛皮、羽绒及制品业",
            "C20" => "木材加工及竹、藤、棕、草制品业",
            "C21" => "家具制造业",
            "C22" => "造纸及纸制品业",
            "C23" => "印刷业、记录媒介的复制",
            "C24" => "文教体育用品制造业",
            "C25" => "石油加工及炼焦业",
            "C26" => "化学原料及化学制品制造业",
            "C27" => "医药制造业（含生物制造业）",
            "C28" => "化学纤维制造业",
            "C29" => "橡胶制品业",
            "C30" => "塑料制品业",
            "C31" => "非金属矿物制品业",
            "C32" => "黑色金属冶炼及压延加工业",
            "C33" => "有色金属冶炼及压延加工业",
            "C34" => "金属制品业（含日用金属制品业）",
            "C35" => "普通机械制造业",
            "C36" => "专用设备制造业",
            "C37" => "交通运输设备制造业",
            "C39" => "武器弹药制造业",
            "C40" => "电气机械及器材制造业",
            "C41" => "电子及通信设备制造业",
            "C42" => "仪器仪表及文化、办公用机械制造业",
            "C43" => "其他制造业"
        ),
        "电力、煤气及水的生产和供应业" => array(
            "D44" => "电力、蒸气、水的生产和供应业",
            "D45" => "煤气生产和供应业",
            "D46" => "自来水的生产和供应业"
        ),
        "建筑业" => array(
            "E47" => "土木工程建筑业",
            "E48" => "线路、管道和设备安装业",
            "E49" => "装修装饰业"
        ),
        "地质勘查业、水利管理业" => array(
            "F50" => "地质勘查业",
            "F51" => "水利管理业"
        ),
        "交通运输业、仓储及邮电通信业" => array(
            "G52" => "铁路运输业",
            "G53" => "公路运输业",
            "G54" => "管道运输业",
            "G55" => "水上运输业",
            "G56" => "航空运输业",
            "G57" => "交通运输辅助业",
            "G58" => "其他交通运输业",
            "G59" => "仓储业",
            "G60" => "邮电通信业"
        ),
        "批发和零售贸易、餐饮业" => array(
            "H61" => "仪器、饮料、烟草和家庭日用品批发业",
            "H62" => "能源、材料和机械电子设备批发业",
            "H63" => "其他批发业",
            "H64" => "零售业",
            "H65" => "商业经纪与代理业",
            "H67" => "餐饮业"
        ),
        "金融、保险业" => array(
            "I68" => "金融业",
            "I70" => "保险业"
        ),
        "房地产业" => array(
            "J72" => "房地产开发与经营业",
            "J73" => "房地产管理业",
            "J74" => "房地产经纪与代理业"
        ),
        "社会服务业" => array(
            "K75" => "公共设施服务业",
            "K76" => "居民服务业",
            "K78" => "旅馆业",
            "K79" => "租赁服务业",
            "K80" => "旅游业",
            "K81" => "娱乐服务业",
            "K82" => "信息、咨询服务业",
            "K83" => "计算机应用服务业",
            "K84" => "其他社会服务业"
        ),
        "卫生、体育和社会福利业" => array(
            "L85" => "卫生",
            "L86" => "体育",
            "L87" => "社会福利保障业"
        ),
        "教育、文化艺术及广播电影电视业" => array(
            "M89" => "教育",
            "M90" => "文化艺术业",
            "M91" => "广播电影电视业"
        ),
        "科学研究和综合技术服务业" => array(
            "N92" => "科学研究业",
            "N93" => "综合技术服务业（含气象、地震、测绘等）"
        ),
        "国家机关、政党机关和社会团体" => array(
            "O94" => "国家机关",
            "O95" => "政党机关",
            "O96" => "社会团体",
            "O97" => "基层群众自治组织"
        ),
        "其他行业" => array(
            "P99" => "其他行业"
        )
    );
}
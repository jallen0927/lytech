<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 24/04/16
 * Time: 19:48
 */
class UnapprovedMember extends DataObject {

    private static $singular_name = '待审核账号';

    private static $plural_name = '待审核账号';

    private static $db = array(
        'Email' => 'Varchar',
        'Password' => 'Varchar',
        'OrganizationName' => 'Varchar(255)',
        'Phone' => 'Varchar',
        'FullName' => 'Varchar',
        'IDCard' => 'Varchar',
        'MemberType' => 'Enum(array("Organization", "Personal"))',
        'Approved' => 'Boolean(false)'
    );

    private static $summary_fields = array(
        'getTitle' => '名称',
        'getMemberTypeLabel' => '账号类型',
        'Email' => '电子邮件'
    );

    private static $has_one = array(
        'Organization' => 'Organization'
    );

    private static $field_labels = array(
        'Email' => '电子邮件',
        'OrganizationName' => '单位名称',
        'Phone' => '电话号码',
        'FullName' => '姓名',
        'IDCard' => '身份证号',
        'MemberType' => '账号类型',
        'Organization' => '单位'
    );

    public function getTitle() {
        return $this->MemberType === 'Personal' ? $this->FullName : $this->OrganizationName;
    }

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('Password');
        $fields->removeByName('Approved');
        $fields->replaceField('MemberType', DropdownField::create('MemberType', '账号类型', array(
            'Organization' => '单位账号',
            'Personal' => '个人账号'
        )));

        if ($this->MemberType === 'Personal') {
            $fields->removeByName('OrganizationName');
        } else {
            $fields->removeByName('FullName');
            $fields->removeByName('IDCard');
            $fields->removeByName('OrganizationID');
        }

        if(Permission::check('ADMIN')) {
            return parent::getCMSFields();
        }

        return $fields;
    }

    public function getMemberTypeLabel() {
        return $this->MemberType === 'Personal' ? '个人账号' : '单位账号';
    }

    public function canView($member = null) {
        if(Permission::check('ADMIN')) {
            return true;
        }

        if ($this->MemberType === 'Organization' && Member::currentUser()->inGroup('admin-office')) {
            return true;
        }

        if ($this->MemberType === 'Personal' && Member::currentUser()->inGroup('Organization')) {
            return true;
        }

        return false;
    }

    public function canApprove($member = null) {
        return $this->canView($member);
    }

    public function doApprove() {
        $exist = Member::get()->filter(array('Email' => $this->Email))->first();
        if($exist) {
            return false;
        }
        $member = new Member();
        $data = $this->toMap();
        unset($data['ID']);
        unset($data['ClassName']);
        unset($data['UnapprovedMember']);
        $member->update($data);
        $member->write();

        if ($this->MemberType === 'Organization') {
            $member->addToGroupByCode('organization');
            $organization = new Organization();
            $organization->AccountID = $member->ID;
            $organization->company_name = $this->OrganizationName;
            $organizationID = $organization->write();
            $member->OrganizationID = $organizationID;
            $member->write();
        } else {
            $member->addToGroupByCode('personal');
        }

        $this->setField('Approved', true);
        $this->write();
        return true;
    }

    public function canDelete($member = null) {
        return true;
    }

}
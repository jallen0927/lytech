<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 20/03/16
 * Time: 20:56
 */
class Schedule extends DataObject {
    private static $singular_name = '进度目标';

    private static $plural_name = '进度目标';

    private static $db = array(
        'StartDate' => 'Date',
        'EndDate' => 'Date',
        'Goal' => 'Text'
    );

    private static $field_labels = array(
        'StartDate' => '开始时间',
        'EndDate' => '结束时间',
        'Goal' => '进度目标要求',
        'Application' => '项目申请'
    );

    private static $default_sort = 'StartDate';

    private static $summary_fields = array(
        'StartDate' => '开始时间',
        'EndDate' => '结束时间',
        'Goal.FirstSentence' => '进度目标要求'
    );

    private static $has_one = array(
        'Application' => 'Application'
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->dataFieldByName('StartDate')->setConfig('showcalendar', true);
        $fields->dataFieldByName('EndDate')->setConfig('showcalendar', true);
        $fields->dataFieldByName('Goal')->setRows(2)->setDescription('每栏限80字');

        return $fields;
    }

    public function getTitle() {
        return $this->StartDate;
    }

    public function canView($member = null) {
        return true;
    }

    public function canCreate($member = null) {
        return true;
    }

    public function canEdit($member = null) {
        return true;
    }

    public function canDelete($member = null) {
        return true;
    }
}
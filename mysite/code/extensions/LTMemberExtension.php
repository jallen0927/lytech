<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 9/10/15
 * Time: 20:04
 */
class LTMemberExtension extends DataExtension {

    private static $db = array(
        'Phone' => 'Varchar',
        'FullName' => 'Varchar',
        'IDCard' => 'Varchar',
        'MemberType' => 'Enum(array("Organization", "Personal"))'
    );

    private static $has_one = array(
        'Organization' => 'Organization'
    );

    public function requireDefaultRecords() {
        parent::requireDefaultRecords(); 
        $groups = Config::inst()->get('Group', 'default_groups');
        foreach ($groups as $group) {
            $groupObj = Group::singleton()->findOrCreate(array('Code' => $group['code']));
            $groupObj->Title = $group['title'];
            $groupObj->write();

            if(!empty($group['permissions'])) {
                foreach ($group['permissions'] as $permission) {
                    if(!in_array($permission, $groupObj->Permissions()->column('Code'))) {
                        Permission::grant($groupObj->ID, $permission);
                        DB::alteration_message('Permission ' . $permission . ' has been granted to group ' . $groupObj->Title, 'created');
                    }
                }
            }
        }

    }

    public function getLTGroupConfig() {
        $groupCode = $this->owner->Groups()->first()->Code;
        $groupConfigs = Config::inst()->get('Group', 'default_groups');
        $groupConfigKey = array_search($groupCode, array_column($groupConfigs, 'code'));

        if ($groupConfigKey !== false && array_key_exists($groupConfigKey, $groupConfigs)) {

            return $groupConfigs[$groupConfigKey];
        }

        return false;
    }

    public function findOrCreateOrganization() {
        $organization = $this->owner->Organization();
        if($organization->ID) {
            return $organization;
        }

        $organization = new Organization();
        $organization->write();
        $organization->Members()->add($this->owner);

        return $organization;
    }
}
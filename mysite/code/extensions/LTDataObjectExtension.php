<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 22/03/16
 * Time: 22:13
 */
class LTDataObjectExtension extends DataExtension {
    public function findOrCreate(Array $array) {
        $obj = DataObject::get_one($this->owner->ClassName, $array);
        if (!$obj) {
            $obj = new $this->owner->ClassName();
            $obj->update($array);
            $obj->write();
        }

        return $obj;
    }
}
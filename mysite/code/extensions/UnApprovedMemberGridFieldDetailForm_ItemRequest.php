<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 24/04/16
 * Time: 21:08
 */
class UnApprovedMemberGridFieldDetailForm_ItemRequest extends Extension {

    private static $allowed_actions = array(
        'edit',
        'view',
        'ItemEditForm'
    );

    private static $url_handlers = array(
        '$Action!' => '$Action',
        '' => 'edit',
    );

    public function updateItemEditForm($form) {
        if ($this->owner->record->ClassName !== 'UnapprovedMember') {
            return $form;
        }
        $actions = $form->Actions();
        if ($this->owner->record->canApprove()) {
            $actions->push(FormAction::create('doApprove', '通过审核')
                ->setUseButtonTag(true)
                ->addExtraClass('ss-ui-action-constructive')
                ->setAttribute('data-icon', 'accept'));
        }
    }

    public function doApprove($data, $form) {
        if(!$this->owner->record->canApprove()) {
            return Controller::curr()->httpError(403);
        }
        $controller = Controller::curr();
        $result = $this->owner->record->doApprove();

        if(!$result) {
            $form->sessionMessage('操作失败,可能该电子邮件已被占用', 'bad', false);
            $responseNegotiator = new PjaxResponseNegotiator(array(
                'CurrentForm' => function() use(&$form) {
                    return $form->forTemplate();
                },
                'default' => function() use(&$controller) {
                    return $controller->redirectBack();
                }
            ));
            if($controller->getRequest()->isAjax()){
                $controller->getRequest()->addHeader('X-Pjax', 'CurrentForm');
            }
            return $responseNegotiator->respond($controller->getRequest());
        }

        $redirectLink = 'admin/member-approval';
        Controller::curr()->getResponse()->addHeader("X-Pjax","Content");
        return Controller::curr()->redirect($redirectLink);
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 29/03/16
 * Time: 21:00
 */
class ApplicationGridFieldDetailForm_ItemRequest extends Extension {

    private static $allowed_actions = array(
        'edit',
        'view',
        'ItemEditForm'
    );

    private static $url_handlers = array(
        '$Action!' => '$Action',
        '' => 'edit',
    );

    public function updateItemEditForm($form) {
        if ($this->owner->record->ClassName !== 'Application') {
            return $form;
        }
        $actions = $form->Actions();

        $canSubmitOrg = $this->owner->record->canSubmitOrg();
        $canReturnRevising = $this->owner->record->canReturnRevising();
        $canOfficeApprove = $this->owner->record->canOfficeApprove();
        $canAdminApprove = $this->owner->record->canAdminApprove();
        $canInitiate = $this->owner->record->canInitiate();
        $canFail = $this->owner->record->canFail();

        if($canSubmitOrg) {
            $actions->push(FormAction::create('doSubmitOrg', '上报企业')
                ->setUseButtonTag(true)
                ->addExtraClass('ss-ui-action-constructive')
                ->setAttribute('data-icon', 'accept'));
        }

        if($canOfficeApprove) {
            $actions->push(FormAction::create('doOfficeApprove', '初审通过')
                ->setUseButtonTag(true)
                ->addExtraClass('ss-ui-action-constructive')
                ->setAttribute('data-icon', 'accept'));
        }

        if($canAdminApprove) {
            $actions->push(FormAction::create('doAdminApprove', '审核通过')
                ->setUseButtonTag(true)
                ->addExtraClass('ss-ui-action-constructive')
                ->setAttribute('data-icon', 'accept'));
        }

        if($canReturnRevising) {
            $actions->push(FormAction::create('doReturnRevising', '退回修改')
                ->setUseButtonTag(true)
                ->addExtraClass('ss-ui-action-destructive'));
        }

        if($canInitiate) {
            $actions->push(FormAction::create('doInitiate', '立项')
                ->setUseButtonTag(true)
                ->addExtraClass('ss-ui-action-constructive')
                ->setAttribute('data-icon', 'accept'));
        }

        if($canFail) {
            $actions->push(FormAction::create('doFail', '审核落选')
                ->setUseButtonTag(true)
                ->addExtraClass('ss-ui-action-destructive'));
        }
        
    }

    public function doSubmitOrg($data, $form) {
        if(!$this->owner->record->canSubmitOrg()) {
            return Controller::curr()->httpError(403);
        }

        $this->owner->record->submitOrg();
        $form->sessionMessage('上报项目"' . $this->owner->record->Name . '"成功', 'good', false);

        $redirectLink = Controller::join_links($this->owner->gridField->Link(), "item", $this->owner->record->ID . '?s=PENDING_ORG_ASSESSING');
        Controller::curr()->getResponse()->addHeader("X-Pjax","Content");
        return Controller::curr()->redirect($redirectLink);
    }

    public function doOfficeApprove($data, $form) {
        if(!$this->owner->record->canOfficeApprove()) {
            return Controller::curr()->httpError(403);
        }

        $this->owner->record->officeApprove();
        $form->sessionMessage('项目"' . $this->owner->record->Name . '"初审通过', 'good', false);

        $redirectLink = Controller::join_links($this->owner->gridField->Link(), "item", $this->owner->record->ID . '?s=PENDING_ADMIN_ASSESSING');
        Controller::curr()->getResponse()->addHeader("X-Pjax","Content");
        return Controller::curr()->redirect($redirectLink);
    }

    public function doAdminApprove($data, $form) {
        if(!$this->owner->record->canAdminApprove()) {
            return Controller::curr()->httpError(403);
        }

        $this->owner->record->adminApprove();
        $form->sessionMessage('项目"' . $this->owner->record->Name . '"审核通过', 'good', false);

        $redirectLink = Controller::join_links($this->owner->gridField->Link(), "item", $this->owner->record->ID . '?s=APPROVED');
        Controller::curr()->getResponse()->addHeader("X-Pjax","Content");
        return Controller::curr()->redirect($redirectLink);
    }

    public function doReturnRevising($data, $form) {
        if(!$this->owner->record->canReturnRevising()) {
            return Controller::curr()->httpError(403);
        }

        $this->owner->record->returnRevising();
        $form->sessionMessage('项目"' . $this->owner->record->Name . '"退回修改成功', 'good', false);

        $redirectLink = Controller::join_links($this->owner->gridField->Link(), "item", $this->owner->record->ID . '?s=REVISING');
        Controller::curr()->getResponse()->addHeader("X-Pjax","Content");
        return Controller::curr()->redirect($redirectLink);
    }

    public function doInitiate($data, $form) {
        if(!$this->owner->record->canInitiate()) {
            return Controller::curr()->httpError(403);
        }

        $this->owner->record->initiate();
        $form->sessionMessage('项目"' . $this->owner->record->Name . '"立项成功', 'good', false);

        $redirectLink = Controller::join_links($this->owner->gridField->Link(), "item", $this->owner->record->ID . '?s=INITIATED');
        Controller::curr()->getResponse()->addHeader("X-Pjax","Content");
        return Controller::curr()->redirect($redirectLink);
    }

    public function doFail($data, $form) {
        if(!$this->owner->record->canFail()) {
            return Controller::curr()->httpError(403);
        }

        $this->owner->record->fail();
        $form->sessionMessage('项目"' . $this->owner->record->Name . '"已落选', 'good', false);

        $redirectLink = Controller::join_links($this->owner->gridField->Link(), "item", $this->owner->record->ID . '?s=UNSUCCESSFUL');
        Controller::curr()->getResponse()->addHeader("X-Pjax","Content");
        return Controller::curr()->redirect($redirectLink);
    }

}
<?php

/**
 * Created by PhpStorm.
 * User: xlin
 * Date: 8/04/16
 * Time: 20:12
 */

class RegisterPage_Controller extends Page_Controller {

    private static $allowed_actions = array(
        'organization',
        'personal',
        'OrganizationRegisterForm',
        'PersonalRegisterForm'
    );

    public function init() {
        parent::init();
        Requirements::javascript(implode(DIRECTORY_SEPARATOR, array(SSViewer::get_theme_folder(), 'dist/bundle.js')));
    }

    public function index() {
        return $this->renderWith(array('RegisterPage', 'Page'));
    }

    public function organization() {
        return $this;
    }

    public function personal() {
        return $this;
    }

    public function OrganizationRegisterForm() {
        $fields = FieldList::create(array(
            EmailField::create('Email', '电子邮件'),
            ConfirmedPasswordField::create('Password', '密码'),
            TextField::create('OrganizationName', '单位名称'),
            TextField::create('Phone', '联系电话')
        ));

        $actions = FieldList::create(array(
            FormAction::create('doRegisterOrganization', '提交')
        ));

        $required = RequiredFields::create(array(
            'Email',
            'Password',
            'OrganizationName',
            'Phone'
        ));

        $form = new Form($this, __FUNCTION__, $fields, $actions, $required);

        return $form;
    }

    public function doRegisterOrganization(Array $data, Form $form) {
        $exist = Member::get()->filter(array('Email' => $this->Email))->first();
        if($exist) {
            $form->sessionMessage('该电子邮件已被注册', 'bad');
            return $this->redirectBack();
        }
        $member = new UnapprovedMember();
        $form->saveInto($member);
        $member->setField('MemberType', 'Organization');
        $member->write();

        $form->sessionMessage('注册成功，请等待管理员审核账号，审核通过之后可以正常登陆', 'good');
        $this->redirectBack();
    }

    public function PersonalRegisterForm() {
        $fields = FieldList::create(array(
            EmailField::create('Email', '电子邮件'),
            ConfirmedPasswordField::create('Password', '密码'),
            TextField::create('FullName', '姓名'),
            TextField::create('IDCard', '身份证号码'),
            TextField::create('Phone', '联系电话'),
            DropdownField::create(
                'OrganizationID',
                '所属单位名称',
                Organization::get()->map('ID', 'company_name')
            )->setEmptyString('请选择')
        ));

        $actions = FieldList::create(array(
            FormAction::create('doRegisterPersonal', '提交')
        ));

        $required = RequiredFields::create(array(
            'Email',
            'Password',
            'FullName',
            'IDCard',
            'Phone',
            'OrganizationID'
        ));

        $form = new Form($this, __FUNCTION__, $fields, $actions, $required);

        return $form;
    }

    public function doRegisterPersonal(Array $data, Form $form) {
        $exist = Member::get()->filter(array('Email' => $this->Email))->first();
        if($exist) {
            $form->sessionMessage('该电子邮件已被注册', 'bad');
            return $this->redirectBack();
        }
        $member = new UnapprovedMember();
        $form->saveInto($member);
        $member->setField('MemberType', 'Personal');
        $member->write();

        $form->sessionMessage('注册成功，请等待您所属的企业审核账号，审核通过之后可以正常登陆', 'good');
        return $this->redirectBack();
    }

    public function Link($action = null) {
        return 'register';
    }

}